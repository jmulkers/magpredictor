
predict: main.o mag.o
	g++ -o $@ main.o mag.o -L/usr/lib/x86_64-linux-gnu/ -lcudart

main.o: main.cpp
	g++ -c main.cpp

mag.o: mag.cu
	nvcc -c mag.cu

clean:
	rm predict
	rm *.o
