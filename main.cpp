#include "mag.h"

int main(){

    float A = 15e-12;
    float K = 0.8e6;
    float D = 6e-3;
    float dx = 1e-9;
    float dy = 1e-9;
    float dz = 1e-9;
    float Msat = 580e3;
    int nx = 200;
    int ny = 300;
    int nz = 1;
    int pbcx = 0;
    int pbcy = 0;
    int pbcz = 0;
    float Bx = 0.;
    float By = 0.;
    float Bz = 0.;

    Mag m(nx,ny,nz);
    m.setpbc(pbcx,pbcy,pbcz);
    m.setcellsize(dx,dy,dz);
    m.setmatprop(Msat,A,D,K);
    m.setextfield(Bx,By,Bz);
    m.setanis(0,0,1);

    m.setrandommag(10);
    m.predict(100000);
    m.printfile();

    return 0;
}

