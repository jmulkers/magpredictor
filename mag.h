#ifndef MAG_H
#define MAG_H

class Mag {
    public:
        Mag( int _nx, int _ny, int _nz);
        ~Mag();
        void setcellsize(float _dx=1e9f, float _dy=1e9f, float _dz=1e9f);
        void setpbc(int _pbcx=0, int _pbcy=0, int _pbcz=0);
        void setmatprop(float _Msat=580e3, float _A=15e-12, float _D=6e-3, float _K=0.8e6 );
        void setextfield(float Bx=0.f, float By=0.f, float Bz=0.f);
        void setrandommag(int seed);
        void setanis(float kx, float ky, float kz);
        void predict(int nstep);
        void printfile();
    private:
        void setgridsize(int _nx, int _ny, int _nz);
        int nx,ny,nz,N;
        int pbcx, pbcy, pbcz;
        float dx,dy,dz;
        float Msat, A, K, D;
        float Bx, By, Bz;
        float *mx,*my,*mz;
        float *krot;
};

#endif
