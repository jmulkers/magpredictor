#include <stdio.h>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include"mag.h"
#include"curand.h"
#include"curand_kernel.h"


__device__ inline int idx(int ix, int iy, int iz, int nx, int ny, int nz){
    return  (abs(iz%nz)*(ny) +abs(iy%ny)) *nx +abs(ix%nx);
}

__global__ void setup_kernel ( curandState * state, unsigned long seed, int N){
    int i = threadIdx.x;
    curand_init ( seed, i, 0, &state[i] );
}

__global__ void randommag( curandState * state , float * mx, float * my, float * mz, int N) {
    int tid = threadIdx.x;
    int nrs = blockDim.x;
    int toset = (N-1)/nrs + 1;
    for ( int i = tid*toset; i < (tid+1)*toset; i++) {
        float mxi = 2.f*curand_uniform(&state[tid])-1.;
        float myi = 2.f*curand_uniform(&state[tid])-1.;
        float mzi = 2.f*curand_uniform(&state[tid])-1.;
        float L = sqrt(mxi*mxi+myi*myi+mzi*mzi);
        mx[i] = mxi/L;
        my[i] = myi/L;
        mz[i] = mzi/L;
    }
}

__global__ void predict_kernel(
        float *mx, float *my, float *mz,
        int nx, int ny, int nz, float dx, float dy, float dz,
        float Msat, float A, float K, float D, float *krot,
        float Bx, float By, float Bz,
        int pbcx, int pbcy, int pbcz, int p ){

    int ix = blockIdx.x*blockDim.x + threadIdx.x;
    int iy = blockIdx.y*blockDim.y + threadIdx.y;
    int iz = blockIdx.z*blockDim.z + threadIdx.z;

    if (ix >= nx || iy >= ny || iz>= nz)
        return;

    // switch checkerboard
    if ((ix+iy+iz)%2==p%2)
        return;

    // index of the central cell and his neighbours
    int i = idx(ix,iy,iz,nx,ny,nz);
    int ilx = idx(ix-1,iy,iz,nx,ny,nz);
    int ihx = idx(ix+1,iy,iz,nx,ny,nz);
    int ily = idx(ix,iy-1,iz,nx,ny,nz);
    int ihy = idx(ix,iy+1,iz,nx,ny,nz);
    int ilz = idx(ix,iy,iz-1,nx,ny,nz);
    int ihz = idx(ix,iy,iz+1,nx,ny,nz);

    // some constants
    float Ax = A/(dx*dx);
    float Ay = A/(dy*dy);
    float Az = A/(dz*dz);
    float Dx = D/(2.f*dx);
    float Dy = D/(2.f*dy);

    // effective magnetic field (disregarding anisotropy)
    float bx = 0.f;
    float by = 0.f;
    float bz = 0.f;

    // magnetisation of neighbouring cell j
    float mxj, myj, mzj;

    // neighbour ilx
    if (ix-1 >= 0 || pbcx){
        mxj = mx[ilx];
        myj = my[ilx];
        mzj = mz[ilx];
    } else {
        mxj = mx[ihx] - 2.f*dx*(-mz[ihx]*0.5f*D/A);
        myj = my[ihx];
        mzj = mz[ihx] - 2.f*dx*( mx[ihx]*0.5f*D/A);
    }
    bx += Ax*mxj - Dx*mzj;
    by += Ax*myj;
    bz += Ax*mzj + Dx*mxj;

    // neighbour ihx
    if (ix+1 < nx || pbcx){
        mxj = mx[ihx];
        myj = my[ihx];
        mzj = mz[ihx];
    } else {
        mxj = mx[ilx] + 2.f*dx*(-mz[ilx]*0.5f*D/A);
        myj = my[ilx];
        mzj = mz[ilx] + 2.f*dx*( mx[ilx]*0.5f*D/A);
    }
    bx += Ax*mxj + Dx*mzj;
    by += Ax*myj;
    bz += Ax*mzj - Dx*mxj;

    // neighbour ily
    if (iy-1 >= 0 || pbcy){
        mxj = mx[ily];
        myj = my[ily];
        mzj = mz[ily];
    } else {
        mxj = mx[ihy];
        myj = my[ihy] - 2.f*dy*(-mz[ihy]*0.5f*D/A);
        mzj = mz[ihy] - 2.f*dy*( my[ihy]*0.5f*D/A);
    }
    bx += Ay*mxj;
    by += Ay*myj - Dy*mzj;
    bz += Ay*mzj + Dy*myj;

    // neighbour ihy
    if (iy+1 < ny || pbcy){
        mxj = mx[ihy];
        myj = my[ihy];
        mzj = mz[ihy];
    } else {
        mxj = mx[ily];
        myj = my[ily] + 2.f*dy*(-mz[ily]*0.5f*D/A);
        mzj = mz[ily] + 2.f*dy*( my[ily]*0.5f*D/A);
    }
    bx += Ay*mxj;
    by += Ay*myj + Dy*mzj;
    bz += Ay*mzj - Dy*myj;

    // neighbour ilz
    if (iz-1 >= 0 || pbcz){
        bx += Az*mx[ilz];
        by += Az*my[ilz];
        bz += Ay*mz[ilz];
    }

    // neighbour ihz
    if (iz+1 < nz || pbcz){
        bx += Az*mx[ihz];
        by += Az*my[ihz];
        bz += Az*mz[ihz];
    }

    // add zeeman energy
    bx += Msat*Bx;
    by += Msat*By;
    bz += Msat*Bz;

    // rotate easy axis to e_z
    float rbx =   krot[0]*bx + krot[3]*by + krot[5]*bz;
    float rby =   krot[3]*bx + krot[1]*by + krot[4]*bz;
    float rbz = - krot[5]*bx - krot[4]*by + krot[2]*bz;

    // self consistent calculation to include anisotropy
    float rbx2 = rbx*rbx;
    float rby2 = rby*rby;
    float rbz2 = rbz*rbz;
    float L = sqrt(rbx2+rby2+rbz2);
    float Lprev = L;
    for (int i=0; i<100; i++){
        L = sqrt( rbx2 + rby2 + rbz2/pow(1-K/Lprev,2) );
        if (abs(L-Lprev)/L<1e-5f)
            break;
        Lprev = L;
    }

    // warped normalisation
    float rmxi = rbx/L;
    float rmyi = rby/L;
    float rmzi = rbz/(L-K);

    // rotate back
    float mxi =   krot[0]*rmxi + krot[3]*rmyi - krot[5]*rmzi;
    float myi =   krot[3]*rmxi + krot[1]*rmyi - krot[4]*rmzi;
    float mzi =   krot[5]*rmxi + krot[4]*rmyi + krot[2]*rmzi;

    // a final normalisation to be sure
    L = sqrt(mxi*mxi+myi*myi+mzi*mzi);
    mx[i] = mxi/L;
    my[i] = myi/L;
    mz[i] = mzi/L;
}

Mag::Mag(int _nx, int _ny, int _nz){
    setgridsize(_nx,_ny,_nz);
    setcellsize();
    setpbc();
    setmatprop();
    setextfield();
    cudaMalloc(&mx, N*sizeof(float));
    cudaMalloc(&my, N*sizeof(float));
    cudaMalloc(&mz, N*sizeof(float));

}

Mag::~Mag(){
    cudaFree(mx);
    cudaFree(my);
    cudaFree(mz);

}

void Mag::setgridsize(int _nx, int _ny, int _nz){
    nx = _nx;
    ny = _ny;
    nz = _nz;
    N = nx*ny*nz;
}

void Mag::setcellsize(float _dx, float _dy, float _dz){
    dx = _dx;
    dy = _dy;
    dz = _dz;
}

void Mag::setpbc(int _pbcx, int _pbcy, int _pbcz){
    pbcx = _pbcx;
    pbcy = _pbcy;
    pbcz = _pbcz;
}

void Mag::setmatprop(float _Msat, float _A, float _D, float _K){
    Msat = _Msat;
    A = _A;
    K = _K;
    D = _D;
}

void Mag::setextfield(float _Bx, float _By, float _Bz){
    Bx = _Bx;
    By = _By;
    Bz = _Bz;
}

void Mag::setanis(float kx, float ky, float kz){
    // (u,v,0) rotation vector
    float l = sqrt(kx*kx+ky*ky);
    float u = 0;
    float v = 1;
    if (l != 0){
        u = ky/l;
        v = -kx/l;
    }
    // rotation angle
    kz = kz/sqrt(kx*kx+ky*ky+kz*kz);
    float cosa = kz;
    float sina = -sqrt(1-kz*kz);
    // rotation matrix
    float *rotmat;
    rotmat = (float *)malloc(6*sizeof(float));
    rotmat[0] = u*u+(1-u*u)*cosa;
    rotmat[1] = v*v+(1-v*v)*cosa;
    rotmat[2] = cosa;
    rotmat[3] = u*v*(1-cosa);
    rotmat[4] = -u*sina;
    rotmat[5] = v*sina;
    cudaMalloc(&krot, 6*sizeof(float));
    cudaMemcpy(krot,rotmat,6*sizeof(float),cudaMemcpyHostToDevice);
}

void Mag::setrandommag(int seed){

    int nrs = 512;

    std::cout << "setup random generators" << std::endl;
    curandState *rs;
    cudaMalloc(&rs, nrs*sizeof(curandState));
    setup_kernel<<<1,nrs>>>(rs,seed,N);
    cudaError_t error = cudaGetLastError();
    if(error != cudaSuccess){
        printf("CUDA error: %s\n", cudaGetErrorString(error));
    exit(-1);
    }

    std::cout << "random initial magnetization" << std::endl;
    randommag<<<1,nrs>>>(rs,mx,my,mz,N);
    error = cudaGetLastError();
    if(error != cudaSuccess){
        printf("CUDA error: %s\n", cudaGetErrorString(error));
        exit(-1);
    }

    cudaFree(rs);
}

void Mag::predict(int nstep){
    std::cout << "predicting" << std::endl;
    cudaError_t error = cudaGetLastError();
    int bs = 32;
    dim3 dimBlock(bs,bs,1);
    dim3 dimGrid((nx-1)/bs+1,(ny-1)/bs+1,nz);
    for (int i=0; i<nstep; i++)
        predict_kernel<<<dimGrid, dimBlock>>>(mx,my,mz,nx,ny,nz,dx,dy,dz,Msat,A,K,D,krot,Bx,By,Bz,pbcx,pbcy,pbcz,i%2);
    error = cudaGetLastError();
    if(error != cudaSuccess){
        printf("CUDA error: %s\n", cudaGetErrorString(error));
        exit(-1);
    }
}

void Mag::printfile(){
    std::cout << "copy to host" << std::endl;
    float *hmx, *hmy, *hmz;
    hmx = (float*)malloc(N*sizeof(float));
    hmy = (float*)malloc(N*sizeof(float));
    hmz = (float*)malloc(N*sizeof(float));
    cudaMemcpy( hmx, mx, N*sizeof(float), cudaMemcpyDeviceToHost );
    cudaMemcpy( hmy, my, N*sizeof(float), cudaMemcpyDeviceToHost );
    cudaMemcpy( hmz, mz, N*sizeof(float), cudaMemcpyDeviceToHost );


    std::cout << "printing to file" << std::endl;
    std::ofstream outf("out.txt");
    outf << nx << " " << ny << " " << nz << "\n";
    for(int i=0; i<N; i++)
        outf << hmx[i] << " " << hmy[i] << " " << hmz[i] << std::endl;
    outf.close();

    free(hmx);
    free(hmy);
    free(hmz);
}

